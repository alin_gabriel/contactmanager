package com.bestsoftwareever.contactmanager.events
{
	import com.bestsoftwareever.contactmanager.model.Contact;
	
	import flash.events.Event;

	public class EditContactEvent extends Event
	{
		public static const NAME:String = "editContactEvent";
		public var contact:Contact;
		
		public function EditContactEvent(name:String,contact:Contact)
		{
			super(NAME);
			this.contact = contact;
		}
	}
}