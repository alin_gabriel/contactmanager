package com.bestsoftwareever.contactmanager.events
{
	import flash.events.Event;

	public class AddContactEvent extends Event
	{
		public static const NAME:String = "addContact";
		
		public function AddContactEvent(name:String)
		{
			super(name);
		}
	}
}