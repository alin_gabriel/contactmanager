package com.bestsoftwareever.contactmanager.events
{
	import flash.events.Event;

	public class GetContactEvent extends Event
	{
		public static const NAME:String = "getContact";
		
		public function GetContactEvent(name:String)
		{
			super(name);
		}
	}
}