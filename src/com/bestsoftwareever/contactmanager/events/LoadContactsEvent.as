package com.bestsoftwareever.contactmanager.events
{
	import flash.events.Event;

	public class LoadContactsEvent extends Event
	{
		public static var NAME:String = "loadContacts";
		
		public function LoadContactsEvent(name:String)
		{
			super(NAME);
		}
	}
}