package com.bestsoftwareever.contactmanager.events
{
	import flash.events.Event;

	public class SetViewEvent extends Event
	{
		public static const NAME:String = "SetView";
		public var viewName:String;
		
		public function SetViewEvent(viewName:String)
		{
			this.viewName = viewName;
			super(NAME);
		}
	}
}