package com.bestsoftwareever.contactmanager.events
{
	import com.bestsoftwareever.contactmanager.model.Contact;
	
	import flash.events.Event;
	
	public class StoreDataEvent extends Event
	{
		public static const NAME:String = "StoreData";
		public var eventName:String;
		public var contact:Contact;
		
		public function StoreDataEvent(eventName:String, contact:Contact)
		{
			super(NAME);
			this.eventName = eventName;
			this.contact = contact;
		}
	}
}