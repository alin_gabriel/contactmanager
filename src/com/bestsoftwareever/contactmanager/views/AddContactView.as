package com.bestsoftwareever.contactmanager.views
{
	import com.bestsoftwareever.contactmanager.events.AddContactEvent;
	import com.bestsoftwareever.contactmanager.events.LoadContactsEvent;
	import com.bestsoftwareever.contactmanager.events.SetViewEvent;
	import com.bestsoftwareever.contactmanager.model.Contact;
	import com.bestsoftwareever.contactmanager.model.ContactsModel;
	import com.bestsoftwareever.contactmanager.model.ModelLocator;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;

	public class AddContactView extends View
	{
		public static const NAME:String = "addContactView";
		public var contactModel:Contact = new Contact();
		
		private var addContactLabel:TextField;
		private var firstNameLabel:TextField;
		public var firstName:TextField;
		public var middleName:TextField;
		private var middleNameLabel:TextField;
		public var lastName:TextField;
		private var lastNameLabel:TextField;
		public var age:TextField;
		private var ageLabel:TextField;
		public var phoneNumber:TextField;
		private var phoneNumberLabel:TextField;
		private var professionLabel:TextField;
		public var profession:TextField;
		private var backButton:TextField;
		private var saveButton:TextField;
		private var loadPicture:TextField;
		private var file:File;
		public var filePath:File = null;
		
		public function AddContactView()
		{
			skin = new Sprite();
			super(skin);
			
			addContactLabel = new TextField();
			addContactLabel.defaultTextFormat = new TextFormat("Arial",30,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
			addContactLabel.text = "Add contact";
			addContactLabel.width = 300;
			addContactLabel.height = 35;
			addContactLabel.selectable = false;
			
			firstNameLabel = new TextField();
			firstNameLabel.defaultTextFormat = new TextFormat("Arial",16,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
			firstNameLabel.text = "First name";
			firstNameLabel.width = 100;
			firstNameLabel.height = 20;
			firstNameLabel.x = 0;
			firstNameLabel.y = 80;
			firstNameLabel.selectable = false;
			
			firstName = new TextField();
			firstName.defaultTextFormat = new TextFormat("Arial",14,0x0,null,null,null,null,null,TextFormatAlign.CENTER);
			firstName.text = "";
			firstName.width = 150;
			firstName.height = 20;
			firstName.x = 120;
			firstName.y = 80;
			firstName.border = true;
			firstName.type = "input";
			
			middleNameLabel = new TextField();
			middleNameLabel.defaultTextFormat = new TextFormat("Arial",16,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
			middleNameLabel.text = "Middle name";
			middleNameLabel.width = 100;
			middleNameLabel.height = 20;
			middleNameLabel.x = 0;
			middleNameLabel.y = 130;
			middleNameLabel.selectable = false;
			
			middleName = new TextField();
			middleName.defaultTextFormat = new TextFormat("Arial",14,0x0,null,null,null,null,null,TextFormatAlign.CENTER);
			middleName.text = "";
			middleName.width = 150;
			middleName.height = 20;
			middleName.x = 120;
			middleName.y = 130;
			middleName.border = true;
			middleName.type = "input";
			
			lastNameLabel = new TextField();
			lastNameLabel.defaultTextFormat = new TextFormat("Arial",16,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
			lastNameLabel.text = "Last name";
			lastNameLabel.width = 100;
			lastNameLabel.height = 20;
			lastNameLabel.x = 0;
			lastNameLabel.y = 180;
			lastNameLabel.selectable = false;
			
			lastName = new TextField();
			lastName.defaultTextFormat = new TextFormat("Arial",14,0x0,null,null,null,null,null,TextFormatAlign.CENTER);
			lastName.text = "";
			lastName.width = 150;
			lastName.height = 20;
			lastName.x = 120;
			lastName.y = 180;
			lastName.border = true;
			lastName.type = "input";
			
			ageLabel = new TextField();
			ageLabel.defaultTextFormat = new TextFormat("Arial",16,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
			ageLabel.text = "Age";
			ageLabel.width = 100;
			ageLabel.height = 25;
			ageLabel.x = 0;
			ageLabel.y = 230;
			ageLabel.selectable = false;
			
			age = new TextField();
			age.defaultTextFormat = new TextFormat("Arial",14,0x0,null,null,null,null,null,TextFormatAlign.CENTER);
			age.text = "";
			age.width = 150;
			age.height = 20;
			age.x = 120;
			age.y = 230;
			age.border = true;
			age.type = "input";
			
			phoneNumberLabel = new TextField();
			phoneNumberLabel.defaultTextFormat = new TextFormat("Arial",16,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
			phoneNumberLabel.text = "Phone";
			phoneNumberLabel.width = 100;
			phoneNumberLabel.height = 25;
			phoneNumberLabel.x = 0;
			phoneNumberLabel.y = 280;
			phoneNumberLabel.selectable = false;
			
			phoneNumber = new TextField();
			phoneNumber.defaultTextFormat = new TextFormat("Arial",14,0x0,null,null,null,null,null,TextFormatAlign.CENTER);
			phoneNumber.text = "";
			phoneNumber.width = 150;
			phoneNumber.height = 20;
			phoneNumber.x = 120;
			phoneNumber.y = 280;
			phoneNumber.border = true;
			phoneNumber.type = "input";
			
			professionLabel = new TextField();
			professionLabel.defaultTextFormat = new TextFormat("Arial",16,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
			professionLabel.text = "Profession";
			professionLabel.width = 100;
			professionLabel.height = 25;
			professionLabel.x = 0;
			professionLabel.y = 330;
			professionLabel.selectable = false;
			
			profession = new TextField();
			profession.defaultTextFormat = new TextFormat("Arial",14,0x0,null,null,null,null,null,TextFormatAlign.CENTER);
			profession.text = "";
			profession.width = 150;
			profession.height = 20;
			profession.x = 120;
			profession.y = 330;
			profession.border = true;
			profession.type = "input";
			
			loadPicture = new TextField();
			loadPicture.defaultTextFormat = new TextFormat("Arial",16,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
			loadPicture.text = "Load picture";
			loadPicture.width = 100;
			loadPicture.height = 25;
			loadPicture.x = 150 - 50;
			loadPicture.y = 380;
			loadPicture.selectable = false;
			loadPicture.border = true;
			
			backButton = new TextField();
			backButton.defaultTextFormat = new TextFormat("Arial",16,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
			backButton.text = "Back";
			backButton.width = 80;
			backButton.height = 20;
			backButton.border = true;
			backButton.x = 20;
			backButton.y = 420;
			backButton.selectable = false;
			
			saveButton = new TextField();
			saveButton.defaultTextFormat = new TextFormat("Arial",16,0x0,true,null,null,null,null,TextFormatAlign.CENTER);
			saveButton.text = "Save";
			saveButton.width = 80;
			saveButton.height = 20;
			saveButton.border = true;
			saveButton.x = 300 - 80 - 20;
			saveButton.y = 420;
			saveButton.selectable = false;
			
			skin.addChild(addContactLabel);
			skin.addChild(firstNameLabel);
			skin.addChild(firstName);
			skin.addChild(middleNameLabel);
			skin.addChild(middleName);
			skin.addChild(lastNameLabel);
			skin.addChild(lastName);
			skin.addChild(ageLabel);
			skin.addChild(age);
			skin.addChild(phoneNumberLabel);
			skin.addChild(phoneNumber);
			skin.addChild(professionLabel);
			skin.addChild(profession);
			skin.addChild(loadPicture);
			skin.addChild(backButton);
			skin.addChild(saveButton);
			
			backButton.addEventListener(MouseEvent.CLICK, onBackButtonClick);
			saveButton.addEventListener(MouseEvent.CLICK, onSaveButtonClick);
			loadPicture.addEventListener(MouseEvent.CLICK, onLoadPictureClick);
		}
		
		override protected function initialize(event:Event):void {
			contactModel = new Contact();
			super.initialize(event);
		}
		
		protected function onLoadPictureClick(event:MouseEvent):void
		{
			file = new File();
			var imageFileTypes:FileFilter = new FileFilter("Images (*.jpg, *.png)", "*.jpg;*.png");
			
			file.browse([imageFileTypes]);
			file.addEventListener(Event.SELECT, selectFile);
		}
		
		protected function selectFile(event:Event):void
		{
			file.load();
			file.addEventListener(Event.COMPLETE, onLoadPictureComplete);
		}
		
		protected function onLoadPictureComplete(event:Event):void
		{
			filePath = new File().resolvePath("D:\\Work\\Actionscript\\Contact Manager\\src\\Contacts Pictures\\" + contactModel.id + ".jpg");
			file.copyTo(filePath,true); 
		}
		
		protected function onSaveButtonClick(event:MouseEvent):void
		{	
			dispatcher.dispatchEvent(new AddContactEvent(AddContactEvent.NAME)); 
			dispatcher.dispatchEvent(new SetViewEvent(ContactsList.NAME));
		}
		
		protected function onBackButtonClick(event:MouseEvent):void
		{
			var contactsList:SetViewEvent = new SetViewEvent(ContactsList.NAME);
			dispatcher.dispatchEvent(contactsList);
		}
	}
}