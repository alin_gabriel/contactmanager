package com.bestsoftwareever.contactmanager.views
{
	import com.bestsoftwareever.contactmanager.events.AddContactEvent;
	import com.bestsoftwareever.contactmanager.events.DeleteContactEvent;
	import com.bestsoftwareever.contactmanager.events.GetContactEvent;
	import com.bestsoftwareever.contactmanager.events.SetViewEvent;
	import com.bestsoftwareever.contactmanager.model.Contact;
	import com.bestsoftwareever.contactmanager.model.ContactsModel;
	import com.bestsoftwareever.contactmanager.model.ModelLocator;
	import com.bestsoftwareever.contactmanager.model.ViewsModel;
	import com.bestsoftwareever.contactmanager.views.View;
	
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	public class ContactsList extends View
	{
		public static var NAME:String = "contactsList";
		public var contactsModel:ContactsModel = new ContactsModel();
		public var info:Array = new Array();
		public var addButton:TextField;
		public var editButton:TextField;
		public var contactsLabel:TextField;
		private var deleteButton:TextField;
		
		public function ContactsList()
		{
			skin = new Sprite();
			super(skin);
			
			deleteButton = new TextField();
			deleteButton.defaultTextFormat = new TextFormat("Arial",25,"0x00ff00",true,null,null,null,null,TextFormatAlign.CENTER);
			deleteButton.text = "Delete";
			deleteButton.border = true;
			deleteButton.width = 90;
			deleteButton.height = 35;
			deleteButton.selectable = false;
			deleteButton.visible = false;
			
			addButton = new TextField();
			addButton.defaultTextFormat = new TextFormat("Arial",30,"0x00ff00",true,null,null,null,null,TextFormatAlign.CENTER);
			addButton.text = "Add";
			addButton.border = true;
			addButton.width = 300 - 180;
			addButton.height = 35;
			addButton.x = 90;
			addButton.y = 0;
			addButton.selectable = false;
			
			editButton = new TextField();
			editButton.defaultTextFormat = new TextFormat("Arial",25,"0x00ff00",true,null,null,null,null,TextFormatAlign.CENTER);
			editButton.text = "Edit";
			editButton.border = true;
			editButton.width = 90;
			editButton.height = 35;
			editButton.x = 300 - 90;
			editButton.y = 0;
			editButton.selectable = false;
			editButton.visible = false;
			
			contactsLabel = new TextField();
			contactsLabel.defaultTextFormat = new TextFormat("Arial",25,"0x0",true,null,null,null,null,TextFormatAlign.CENTER);
			contactsLabel.text = "Contacts";
			contactsLabel.x = 90;
			contactsLabel.y = 50;
			contactsLabel.height = 35; 
			contactsLabel.width = 120; 
			contactsLabel.selectable = false;
			
			contactsModel = ModelLocator.getInstance().getModel(ContactsModel.NAME) as ContactsModel;
			
			if ( contactsModel != null ) 
			{
				var x:int = 0, y:int = 100;
				for (var i:int = 0; i < contactsModel.contacts.length; i++) 
				{
					info[i] = new TextField();
					info[i].defaultTextFormat = new TextFormat("Arial",20,"0x0",true,null,null,null,null,TextFormatAlign.CENTER);
					info[i].text = contactsModel.contacts[i].id + " " + contactsModel.contacts[i].firstName + " " + contactsModel.contacts[i].lastName;
					info[i].x = x;
					info[i].y = y;
					info[i].height = 40;
					info[i].width = 299;
					info[i].border = true;
					info[i].selectable = false;
					info[i].background = true;
					info[i].backgroundColor = 0xffffff;
					
					info[i].addEventListener(MouseEvent.CLICK, onContactClick);
					
					skin.addChild(info[i]);
					y += 40;
				}
			}
			
			skin.addChild(addButton);
			skin.addChild(editButton);
			skin.addChild(contactsLabel);
			skin.addChild(deleteButton);
			
			addButton.addEventListener(MouseEvent.CLICK, onAddButtonClick);
			editButton.addEventListener(MouseEvent.CLICK, onEditButtonClick);
			deleteButton.addEventListener(MouseEvent.CLICK, onDeleteButtonClick);
		}
		
		protected function onDeleteButtonClick(event:MouseEvent):void
		{
			if ( contactsModel.selectedContact.firstName == '' && contactsModel.selectedContact.middleName == '' &&
				contactsModel.selectedContact.lastName == '' && contactsModel.selectedContact.age == 0 &&
				contactsModel.selectedContact.profession == '' )
				return;
			
			dispatcher.dispatchEvent(new DeleteContactEvent(DeleteContactEvent.NAME));
		}
		
		protected function onEditButtonClick(event:MouseEvent):void
		{	
			var viewsModel:ViewsModel = ModelLocator.getInstance().getModel(ViewsModel.NAME) as ViewsModel;
			var editContactView:EditContactView = viewsModel.getView(EditContactView.NAME) as EditContactView;
			editContactView.firstName.text = contactsModel.selectedContact.firstName;
			editContactView.middleName.text = contactsModel.selectedContact.middleName;
			editContactView.lastName.text = contactsModel.selectedContact.lastName;
			editContactView.age.text = contactsModel.selectedContact.age.toString();
			editContactView.phoneNumber.text = contactsModel.selectedContact.phoneNumber;
			editContactView.profession.text = contactsModel.selectedContact.profession;
			if ( editContactView.picture.parent != null )
			{
				editContactView.skin.removeChild(editContactView.picture);
				editContactView.picture = new Loader();
				editContactView.picture.load(new URLRequest(contactsModel.selectedContact.picture));
				editContactView.picture.contentLoaderInfo.addEventListener(Event.COMPLETE, editContactView.onLoadComplete);
			}
			//viewsModel.overwriteView(EditContactView.NAME, new EditContactView());
			
			dispatcher.dispatchEvent(new SetViewEvent(EditContactView.NAME));
		}
		
		public function onContactClick(event:MouseEvent):void
		{
			var name:String;
			for (var i:int = 0; i < contactsModel.contacts.length; i++) 
			{
				name = contactsModel.contacts[i].id + " " + contactsModel.contacts[i].firstName + " " + contactsModel.contacts[i].lastName;
				if ( name == event.target.text )
					break;
			}
			for (var j:int = 0; j < info.length; j++) 
				if ( j != i )
					(info[j] as TextField).backgroundColor = 0xffffff;
			if ( (info[i] as TextField).backgroundColor == 0x006fff )
			{
				editButton.visible = false;
				deleteButton.visible = false;
				info[i].backgroundColor = 0xffffff;
				
				var viewsModel:ViewsModel = ModelLocator.getInstance().getModel(ViewsModel.NAME) as ViewsModel;
				var contactDetails:ContactDetails = viewsModel.getView(ContactDetails.NAME) as ContactDetails;
				
				contactDetails.firstName.text = contactsModel.selectedContact.firstName;
				contactDetails.middleName.text = contactsModel.selectedContact.middleName;
				contactDetails.lastName.text = contactsModel.selectedContact.lastName;
				contactDetails.age.text = contactsModel.selectedContact.age.toString();
				contactDetails.phoneNumber.text = contactsModel.selectedContact.phoneNumber;
				contactDetails.profession.text = contactsModel.selectedContact.profession;
				if ( contactDetails.picture != null )
					contactDetails.skin.removeChild(contactDetails.picture);
				contactDetails.picture = new Loader();
				contactDetails.picture.load(new URLRequest(contactsModel.selectedContact.picture));
				contactDetails.picture.contentLoaderInfo.addEventListener(Event.COMPLETE, contactDetails.onLoadComplete);
				
				dispatcher.dispatchEvent(new SetViewEvent(ContactDetails.NAME));
			}
			else
			{
				deleteButton.visible = true;
				editButton.visible = true;
				(info[i] as TextField).backgroundColor = 0x006fff;
				contactsModel.selectedContact = contactsModel.contacts[i];
			}
		}
		
		protected function onAddButtonClick(event:MouseEvent):void
		{
			var viewsModel:ViewsModel = ModelLocator.getInstance().getModel(ViewsModel.NAME) as ViewsModel;
			var addContactView:AddContactView = viewsModel.getView(AddContactView.NAME) as AddContactView;
			
			addContactView.firstName.text = "";
			addContactView.middleName.text = "";
			addContactView.lastName.text = "";
			addContactView.age.text = "";
			addContactView.phoneNumber.text = "";
			addContactView.profession.text = "";
			addContactView.filePath = null;
			
			dispatcher.dispatchEvent(new SetViewEvent(AddContactView.NAME));
		}
	}
}