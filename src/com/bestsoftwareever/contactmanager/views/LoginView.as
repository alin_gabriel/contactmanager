package com.bestsoftwareever.contactmanager.views
{
	import flash.display.DisplayObjectContainer;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import org.osmf.elements.ImageLoader;
	import com.bestsoftwareever.contactmanager.events.LoginEvent;

	public class LoginView extends View
	{
		public var loginView:DisplayObjectContainer;
		public var userText:TextField;
		public var passwordText:TextField;
		public var labelUser:TextField;
		public var labelPassword:TextField;
		public var loginImage:Loader;
		
		private var loginEvent:LoginEvent;
		public static var NAME:String = "loginView";
		
		public function LoginView()
		{
			loginView = new Sprite();
			super(loginView);
			
			userText = new TextField();
			userText.defaultTextFormat = new TextFormat("Arial",12,0x0);
			userText.text = "";
			userText.border = true;
			userText.width = 100;
			userText.height = 20;
			userText.x = 100;
			userText.y = 200;
			userText.type = "input";
			
			passwordText = new TextField();
			passwordText.defaultTextFormat = new TextFormat("Arial",12,0x0);
			passwordText.text = "";
			passwordText.border = true;
			passwordText.width = 100;
			passwordText.height = 20;
			passwordText.x = 100;
			passwordText.y = 250;
			passwordText.displayAsPassword = true;
			passwordText.type = "input";
			
			labelUser = new TextField();
			labelUser.defaultTextFormat = new TextFormat("Arial",12,0x0);
			labelUser.text = "Username";
			labelUser.x = 100;
			labelUser.y = 180;
			labelUser.height = 20;
			
			labelPassword = new TextField();
			labelPassword.defaultTextFormat = new TextFormat("Arial",12,0x0);
			labelPassword.text = "Password";
			labelPassword.x = 100;
			labelPassword.y = 230;
			labelPassword.height = 20;
			
			loginImage = new Loader();
			loginImage.load(new URLRequest("/Images/login.jpg"));
			loginImage.contentLoaderInfo.addEventListener(Event.COMPLETE,onLoadComplete);
			
			loginView.addChild(userText);
			loginView.addChild(passwordText);
			loginView.addChild(labelUser);
			loginView.addChild(labelPassword);
			
			loginImage.addEventListener(MouseEvent.CLICK, onClickLoginButton);
		}
		
		protected function onClickLoginButton(event:MouseEvent):void
		{
			loginEvent = new LoginEvent(userText.text,passwordText.text);
			dispatcher.dispatchEvent(loginEvent);
		}
		
		protected function onLoadComplete(event:Event):void
		{
			loginImage.scaleX = loginImage.scaleY = 0.3;
			loginImage.x = 150 - loginImage.content.width * 0.3 / 2;
			loginImage.y = 310;
			loginView.addChild(loginImage);
		}
	}
}