package com.bestsoftwareever.contactmanager.commands
{
	import com.bestsoftwareever.contactmanager.controller.Controller;
	import com.bestsoftwareever.contactmanager.events.SetViewEvent;
	import com.bestsoftwareever.contactmanager.model.Contact;
	import com.bestsoftwareever.contactmanager.model.ContactsModel;
	import com.bestsoftwareever.contactmanager.model.ModelLocator;
	import com.bestsoftwareever.contactmanager.views.ContactsList;
	
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;

	public class LoadContactsCommand extends Command
	{
		
		private var contactsModel:ContactsModel = ModelLocator.getInstance().getModel(ContactsModel.NAME) as ContactsModel;
		public var file:File = File.desktopDirectory.resolvePath("contacts.txt");
		public var stream:FileStream = new FileStream();
		
		public function LoadContactsCommand()
		{
			super();
		}
		
		override public function execute(event:Event):void
		{
			var contact:Contact = new Contact();
			stream.open(file, FileMode.READ);
			var data:String = stream.readUTFBytes(stream.bytesAvailable);
			var info:Array = new Array(), contactInfo:Array = new Array();
			info = data.split("\n");
			for (var i:int = 0; i < info.length; i++) 
			{
				contactInfo = info[i].split(",");
				contact.firstName = contactInfo[0];
				contact.middleName = contactInfo[1];
				contact.lastName = contactInfo[2];
				contact.age = contactInfo[3];
				contact.phoneNumber = contactInfo[4];
				contact.picture = contactInfo[5];
				contactsModel.contacts.push(contact);
			}
			Controller.getInstance().dispatchEvent(new SetViewEvent(ContactsList.NAME));
		}
	}
}