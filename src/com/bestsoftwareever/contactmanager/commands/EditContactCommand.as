package com.bestsoftwareever.contactmanager.commands
{
	import com.bestsoftwareever.contactmanager.events.EditContactEvent;
	import com.bestsoftwareever.contactmanager.events.StoreDataEvent;
	import com.bestsoftwareever.contactmanager.model.Contact;
	import com.bestsoftwareever.contactmanager.model.ContactsModel;
	import com.bestsoftwareever.contactmanager.model.ModelLocator;
	import com.bestsoftwareever.contactmanager.model.ViewsModel;
	import com.bestsoftwareever.contactmanager.views.ContactsList;
	import com.bestsoftwareever.contactmanager.views.EditContactView;
	import com.bestsoftwareever.contactmanager.views.View;
	
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.text.TextField;

	public class EditContactCommand extends Command
	{
		public function EditContactCommand()
		{
		}
		
		override public function execute(event:Event):void
		{
			var contactsModel:ContactsModel = ModelLocator.getInstance().getModel(ContactsModel.NAME) as ContactsModel;
			var selectedContact:Contact = contactsModel.selectedContact;
			var viewsModel:ViewsModel = ModelLocator.getInstance().getModel(ViewsModel.NAME) as ViewsModel;
			var contactsList:ContactsList = viewsModel.getView(ContactsList.NAME) as ContactsList;
			var editContactView:EditContactView = viewsModel.getView(EditContactView.NAME) as EditContactView;
			
			for (var i:int = 0; i < contactsModel.contacts.length; i++) 
				if ( contactsModel.contacts[i] == contactsModel.selectedContact )
					break;
			
			selectedContact.firstName = editContactView.firstName.text;
			selectedContact.middleName = editContactView.middleName.text;
			selectedContact.lastName = editContactView.lastName.text;
			selectedContact.age = parseInt(editContactView.age.text);
			selectedContact.phoneNumber = editContactView.phoneNumber.text;
			selectedContact.profession = editContactView.profession.text;
			
			//Update picture
			editContactView.filePath = new File().resolvePath("D:\\Work\\Actionscript\\Contact Manager\\src\\Contacts Pictures\\" + selectedContact.id + ".jpg");
			editContactView.file.copyTo(editContactView.filePath,true);
			
			contactsModel.contacts[i].update(selectedContact);
			
			contactsList.info[i].text = selectedContact.id + " " + selectedContact.firstName + " " + selectedContact.lastName;
			
			dispatcher.dispatchEvent(new StoreDataEvent(EditContactEvent.NAME, selectedContact));
		}
	}
}