package com.bestsoftwareever.contactmanager.commands
{
	import com.bestsoftwareever.contactmanager.controller.Controller;
	import com.bestsoftwareever.contactmanager.events.LoadContactsEvent;
	import com.bestsoftwareever.contactmanager.events.LoginEvent;
	import com.bestsoftwareever.contactmanager.events.SetViewEvent;
	import com.bestsoftwareever.contactmanager.views.ContactsList;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.URLLoader;
	import flash.net.URLRequest;

	public class LoginCommand extends Command
	{	
		public function LoginCommand()
		{
			super();
		}
		
		override public function execute(event:Event):void
		{
			var file:File = File.applicationStorageDirectory.resolvePath("login.txt");
			var stream:FileStream = new FileStream();
			if ( !file.exists )
			{
				stream.open(file, FileMode.WRITE);
				stream.close();
			}
			var loginEvent:LoginEvent = LoginEvent(event);
			var authData:Array = new Array();
			stream.open(file, FileMode.READ);
			var data:String = stream.readUTFBytes(stream.bytesAvailable);
			var auth:Array = new Array();
			auth = data.split("\n");
			for (var i:int = 0; i < auth.length; i++) 
			{
				authData = auth[i].split(",");
				if ( authData[0] == loginEvent.username )
				{
					if ( authData[1] == loginEvent.password )
					{
						trace("Login successful! Correct user..."); 
						dispatcher.dispatchEvent(new SetViewEvent(ContactsList.NAME));
					}
					else
						trace("Wrong password!");
					break;
				}
			}
			if ( i == auth.length )
			{
				stream.close();
				stream.open(file, FileMode.APPEND);
				stream.writeUTFBytes("\n" + loginEvent.username + "," + loginEvent.password);
				trace("Login successful! Adding user...");
				dispatcher.dispatchEvent(new SetViewEvent(ContactsList.NAME));
				//dispatcher.dispatchEvent(new LoadContactsEvent(LoadContactsEvent.NAME));
			}
			stream.close();
		}
		
	}
}