package com.bestsoftwareever.contactmanager.commands
{
	import com.bestsoftwareever.contactmanager.events.AddContactEvent;
	import com.bestsoftwareever.contactmanager.events.StoreDataEvent;
	import com.bestsoftwareever.contactmanager.model.Contact;
	import com.bestsoftwareever.contactmanager.model.ContactsModel;
	import com.bestsoftwareever.contactmanager.model.ModelBase;
	import com.bestsoftwareever.contactmanager.model.ModelLocator;
	import com.bestsoftwareever.contactmanager.model.ViewsModel;
	import com.bestsoftwareever.contactmanager.views.AddContactView;
	import com.bestsoftwareever.contactmanager.views.ContactsList;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;

	public class AddContactCommand extends Command
	{
		public var contactToAdd:Contact = new Contact();
		
		public function AddContactCommand()
		{
			
		}
		
		override public function execute(event:Event):void
		{
			var viewModel:ViewsModel = ModelLocator.getInstance().getModel(ViewsModel.NAME) as ViewsModel;
			var contactsModel:ContactsModel = ModelLocator.getInstance().getModel(ContactsModel.NAME) as ContactsModel;
			var addContactView:AddContactView = viewModel.getView(AddContactView.NAME) as AddContactView;
			var contactsList:ContactsList = viewModel.getView(ContactsList.NAME) as ContactsList;
			var info:TextField = new TextField();
			
			addContactView.contactModel.firstName = addContactView.firstName.text;
			addContactView.contactModel.middleName = addContactView.middleName.text;
			addContactView.contactModel.lastName = addContactView.lastName.text;
			addContactView.contactModel.age = parseInt(addContactView.age.text);
			addContactView.contactModel.phoneNumber = addContactView.phoneNumber.text;
			addContactView.contactModel.profession = addContactView.profession.text;
			addContactView.contactModel.picture = "D:\\Work\\Actionscript\\Contact Manager\\src\\Contacts Pictures\\no_picture.jpg"
			if ( addContactView.filePath != null )
				addContactView.contactModel.picture = addContactView.filePath.nativePath;
			contactToAdd = addContactView.contactModel;
			contactsModel.contacts.push(contactToAdd);
			
			if ( contactToAdd.firstName == '' && contactToAdd.middleName == '' && contactToAdd.lastName == '' &&
				 contactToAdd.age == 0 && contactToAdd.phoneNumber == '' && contactToAdd.profession == '' )
				return;
			
			info.defaultTextFormat = new TextFormat("Arial",20,"0x0",true,null,null,null,null,TextFormatAlign.CENTER);
			info.text = contactToAdd.id + " " + contactToAdd.firstName + " " + contactToAdd.lastName;
			if ( contactsList.info.length != 0 )
			{
				info.x = contactsList.info[contactsList.info.length - 1].x;
				info.y = contactsList.info[contactsList.info.length - 1].y + 40;
			}
			else
			{
				info.x = 0;
				info.y = 100;
			}
			info.height = 40;
			info.width = 299;
			info.border = true;
			info.selectable = false;
			info.background = true;
			info.backgroundColor = 0xffffff;
			contactsList.info.push(info);
			contactsList.skin.addChild(contactsList.info[contactsList.info.length - 1]);
			contactsList.info[contactsList.info.length - 1].addEventListener(MouseEvent.CLICK, contactsList.onContactClick);
			
			dispatcher.dispatchEvent(new StoreDataEvent(AddContactEvent.NAME, contactToAdd));
		}
	}
}