package com.bestsoftwareever.contactmanager.commands
{
	import com.bestsoftwareever.contactmanager.events.AddContactEvent;
	import com.bestsoftwareever.contactmanager.events.DeleteContactEvent;
	import com.bestsoftwareever.contactmanager.events.EditContactEvent;
	import com.bestsoftwareever.contactmanager.events.StoreDataEvent;
	import com.bestsoftwareever.contactmanager.model.Contact;
	import com.bestsoftwareever.contactmanager.model.ContactsModel;
	import com.bestsoftwareever.contactmanager.model.ModelLocator;
	
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;

	public class StoreDataCommand extends Command
	{
		public function StoreDataCommand()
		{
			super();
		}
		
		override public function execute(event:Event):void
		{
			var selectedContact:Contact = (ModelLocator.getInstance().getModel(ContactsModel.NAME) as ContactsModel).selectedContact;
			var temp:Array = new Array();
			var file:File = File.applicationStorageDirectory.resolvePath("contacts.txt");
			var stream:FileStream = new FileStream();
			var storeData:StoreDataEvent = StoreDataEvent(event);
			
			if ( storeData.eventName == AddContactEvent.NAME )
			{
				stream.open(file, FileMode.APPEND);
				stream.writeUTFBytes(storeData.contact.toString() + "\n");
				stream.close();
			}
			if ( storeData.eventName == EditContactEvent.NAME )
			{
				stream.open(file, FileMode.READ);
				temp = stream.readUTFBytes(stream.bytesAvailable).split("\n");
				stream.close();
				
				stream.open(file, FileMode.WRITE);
				for (var j:int = 0; j < temp.length - 1; j++) 
				{
					if ( temp[j] == selectedContact.toString() )
						temp[j] = storeData.contact.toString();
					stream.writeUTFBytes(temp[j] + "\n");
				}
				stream.close();
			}
			if ( storeData.eventName == DeleteContactEvent.NAME )
			{
				stream.open(file, FileMode.READ);
				temp = stream.readUTFBytes(stream.bytesAvailable).split("\n");
				stream.close();
				
				stream.open(file, FileMode.WRITE);
				if ( temp.length == 1 )
				{
					stream.close();
					return;
				}
				for (var k:int = 0; k < temp.length - 1; k++) 
				{
					if ( temp[k] == selectedContact.toString() )
						continue;
					stream.writeUTFBytes(temp[k] + "\n");
				}
				stream.close();
			}
		}
	}
}