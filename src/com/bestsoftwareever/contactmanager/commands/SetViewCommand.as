package com.bestsoftwareever.contactmanager.commands
{
	import com.bestsoftwareever.contactmanager.controller.Controller;
	import com.bestsoftwareever.contactmanager.events.SetViewEvent;
	import com.bestsoftwareever.contactmanager.model.ModelLocator;
	import com.bestsoftwareever.contactmanager.model.ViewsModel;
	import com.bestsoftwareever.contactmanager.views.View;
	
	import flash.events.Event;

	public class SetViewCommand extends Command {
		
		private var viewsModel:ViewsModel = ModelLocator.getInstance().getModel(ViewsModel.NAME) as ViewsModel;
		private var nextView:View;
		
		public function SetViewCommand()
		{
			super();
		}
		
		override public function execute(event:Event):void
		{
			var setViewEvent:SetViewEvent = SetViewEvent(event);
			if (viewsModel.currentView) viewsModel.currentView.remove();
			nextView = viewsModel.getView(setViewEvent.viewName);
			viewsModel.container.addChild(nextView.skin);
			viewsModel.currentView = nextView;
		}
	}
}