package com.bestsoftwareever.contactmanager.commands
{
	import com.bestsoftwareever.contactmanager.events.DeleteContactEvent;
	import com.bestsoftwareever.contactmanager.events.SetViewEvent;
	import com.bestsoftwareever.contactmanager.events.StoreDataEvent;
	import com.bestsoftwareever.contactmanager.model.ContactsModel;
	import com.bestsoftwareever.contactmanager.model.ModelLocator;
	import com.bestsoftwareever.contactmanager.model.ViewsModel;
	import com.bestsoftwareever.contactmanager.views.ContactsList;
	
	import flash.events.Event;

	public class DeleteContactCommand extends Command
	{
		public function DeleteContactCommand()
		{
		}
		
		override public function execute(event:Event):void
		{
			var contactsModel:ContactsModel = ModelLocator.getInstance().getModel(ContactsModel.NAME) as ContactsModel;
			for (var i:int = 0; i < contactsModel.contacts.length; i++) 
				if ( contactsModel.contacts[i].toString() == contactsModel.selectedContact.toString() )
					break;
			contactsModel.contacts.splice(i,1);
			
			var viewsModel:ViewsModel = ModelLocator.getInstance().getModel(ViewsModel.NAME) as ViewsModel;
			viewsModel.overwriteView(ContactsList.NAME, new ContactsList());
			
			dispatcher.dispatchEvent(new StoreDataEvent(DeleteContactEvent.NAME, contactsModel.selectedContact));
			dispatcher.dispatchEvent(new SetViewEvent(ContactsList.NAME));
		}
	}
}