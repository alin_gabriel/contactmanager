package com.bestsoftwareever.contactmanager.controller
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.utils.Dictionary;
	import com.bestsoftwareever.contactmanager.commands.Command;

	public class Controller extends EventDispatcher
	{
		public var commands:Object = {};
		private static var INSTANCE:Controller;
		
		public static function getInstance():Controller {
			if (!INSTANCE) {
				INSTANCE = new Controller(new SingletonEnforcer());
			}
			return INSTANCE;
		}
		
		public function Controller(se:SingletonEnforcer) {
			if (!se is SingletonEnforcer) {
				throw new Error("Controller cannot be instantiated");
			}
		}
		
		public function addCommand(eventType:String, command:Class):void {
			commands[eventType] = command;
			addEventListener(eventType,executeCommand);
		}
		
		public function executeCommand(event:Event):void
		{
			var commandClass:Class = commands[event.type];
			var command:Command = new commandClass();
			command.dispatcher = this;
			command.execute(event);
		}
	}
}

class SingletonEnforcer {
	
}
