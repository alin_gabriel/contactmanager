package com.bestsoftwareever.contactmanager.model
{
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;

	public class ContactsModel extends ModelBase
	{
		public static var NAME:String = "contactsModel";
		public var contacts:Array = new Array();
		public var selectedContact:Contact = new Contact();
		
		public function ContactsModel() 
		{
			super(NAME);
			
			getContacts();
		}
		
		public function getContacts():void
		{
			var stream:FileStream = new FileStream();
			var file:File = File.applicationStorageDirectory.resolvePath("contacts.txt");
			if (!file.exists) {
				stream.open(file, FileMode.WRITE);
				stream.close();
			}
			var contact:Array = new Array();
			stream.open(file, FileMode.READ);
			var data:String = stream.readUTFBytes(stream.bytesAvailable);
			var info:Array = new Array(), contactInfo:Array = new Array();
			info = data.split("\n");
			for (var i:int = 0; i < info.length - 1; i++) 
			{
				contactInfo = info[i].split(",");
				contact[i] = new Contact();
				contact[i].id = contactInfo[0];
				contact[i].firstName = contactInfo[1];
				contact[i].middleName = contactInfo[2];
				contact[i].lastName = contactInfo[3];
				contact[i].age = contactInfo[4];
				contact[i].phoneNumber = contactInfo[5];
				contact[i].profession = contactInfo[6];
				contact[i].picture = contactInfo[7];
				contacts.push(contact[i]);
			}
			stream.close();
		}
	}
}