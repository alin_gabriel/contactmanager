package com.bestsoftwareever.contactmanager.model
{
	import com.bestsoftwareever.contactmanager.views.View;
	
	import flash.display.DisplayObjectContainer;
	import flash.utils.Dictionary;

	public class ViewsModel extends ModelBase
	{
		
		public static var NAME:String = "viewModel";
		
		public var currentView:View;
		public var container:DisplayObjectContainer;
		private var views:Object = {};
		private var viewFactories:Object = {};
		
		public function ViewsModel()
		{
			super(NAME);
		}
		
		public function getView(viewName:String):View
		{
			if (!views[viewName]) {
				views[viewName] = new viewFactories[viewName]();
			}
			return views[viewName];
		}
		
		public function addView(viewName:String,viewClass:Class):void
		{
			viewFactories[viewName] = viewClass;
		}
		
		public function overwriteView(viewName:String, view:View):void
		{
			views[viewName] = view;
		}
	}
}