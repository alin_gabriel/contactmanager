package com.bestsoftwareever.contactmanager.model
{
	import flash.display.Loader;
	import flash.filesystem.File;

	public class Contact 
	{
		private static var lastId:int = 1;
		//public static const NAME:String = "contactModel";
		public var id:int;
		public var firstName:String = "";
		public var middleName:String = "";
		public var lastName:String = "";
		public var age:uint = 0;
		public var phoneNumber:String = "";
		public var profession:String = "";
		public var picture:String;
		
		public function Contact(id:int = 0)
		{
			if (!id) {
				this.id = lastId;
				lastId++;
			} else {
				this.id = id;
				lastId = Math.max(lastId,id+1);
			}
		}
		
		public function toString():String {
			return id + "," + firstName + "," + middleName + "," + lastName + "," +
				age + "," + phoneNumber + "," + profession + "," + picture;
		}
		
		public function clone():Contact {
			var contact:Contact = new Contact();
			contact.update(this);
			return contact;
		}
		
		public function update(oldContact:Contact):void
		{
			id = oldContact.id;
			firstName = oldContact.firstName;
			middleName = oldContact.middleName;
			lastName = oldContact.lastName;
			age = oldContact.age;
			phoneNumber = oldContact.phoneNumber;
			profession = oldContact.profession;
			picture = oldContact.picture;
		}
	}
}