package 
{
	import com.bestsoftwareever.contactmanager.commands.AddContactCommand;
	import com.bestsoftwareever.contactmanager.commands.DeleteContactCommand;
	import com.bestsoftwareever.contactmanager.commands.EditContactCommand;
	import com.bestsoftwareever.contactmanager.commands.LoadContactsCommand;
	import com.bestsoftwareever.contactmanager.commands.LoginCommand;
	import com.bestsoftwareever.contactmanager.commands.SetViewCommand;
	import com.bestsoftwareever.contactmanager.commands.StoreDataCommand;
	import com.bestsoftwareever.contactmanager.controller.Controller;
	import com.bestsoftwareever.contactmanager.events.AddContactEvent;
	import com.bestsoftwareever.contactmanager.events.DeleteContactEvent;
	import com.bestsoftwareever.contactmanager.events.EditContactEvent;
	import com.bestsoftwareever.contactmanager.events.LoadContactsEvent;
	import com.bestsoftwareever.contactmanager.events.LoginEvent;
	import com.bestsoftwareever.contactmanager.events.SetViewEvent;
	import com.bestsoftwareever.contactmanager.events.StoreDataEvent;
	import com.bestsoftwareever.contactmanager.model.Contact;
	import com.bestsoftwareever.contactmanager.model.ContactsModel;
	import com.bestsoftwareever.contactmanager.model.ModelLocator;
	import com.bestsoftwareever.contactmanager.model.ViewsModel;
	import com.bestsoftwareever.contactmanager.views.AddContactView;
	import com.bestsoftwareever.contactmanager.views.ContactDetails;
	import com.bestsoftwareever.contactmanager.views.ContactsList;
	import com.bestsoftwareever.contactmanager.views.EditContactView;
	import com.bestsoftwareever.contactmanager.views.LoginView;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.filesystem.File;
	
	
	[SWF(width="300", height="500", backgroundColor="#ffffff", frameRate="30")]
	
	public class ContactManager extends Sprite
	{
		private var login:LoginView;
		private var control:Controller;
		
		private var modelLocator:ModelLocator;
		private var vm:ViewsModel;
		
		public function ContactManager()
		{
			stage.align = StageAlign.BOTTOM_LEFT;
			
			modelLocator = ModelLocator.getInstance();
			vm = new ViewsModel();
			
			modelLocator.addModel(ViewsModel.NAME,vm);
			modelLocator.addModel(ContactsModel.NAME,new ContactsModel());
			
			vm.addView(LoginView.NAME,LoginView);
			vm.addView(AddContactView.NAME,AddContactView);
			vm.addView(EditContactView.NAME,EditContactView);
			vm.addView(ContactsList.NAME,ContactsList);
			vm.addView(ContactDetails.NAME,ContactDetails);
			vm.container = this;
			
			//var contactDetails:ContactDetails = vm.getView(ContactDetails.NAME) as ContactDetails;
			
			control = Controller.getInstance();
			control.addCommand(LoginEvent.NAME, LoginCommand);
			control.addCommand(SetViewEvent.NAME, SetViewCommand);
			control.addCommand(LoadContactsEvent.NAME, LoadContactsCommand);
			control.addCommand(AddContactEvent.NAME, AddContactCommand);
			control.addCommand(EditContactEvent.NAME, EditContactCommand);
			control.addCommand(StoreDataEvent.NAME, StoreDataCommand);
			control.addCommand(DeleteContactEvent.NAME, DeleteContactCommand);
			
			control.dispatchEvent(new SetViewEvent(LoginView.NAME));
			
		}
	}
}